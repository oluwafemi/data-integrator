class StudentSerializer < ActiveModel::Serializer
  attributes :id, :admission_no, :last_name, :first_name, :other_name, :email, :phone, :class_name
  has_one :fee
  has_many :other_payments
end