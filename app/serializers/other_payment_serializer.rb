class OtherPaymentSerializer < ActiveModel::Serializer
  attributes :id, :item_code, :payment_log_id, :payment_method,
             :payment_reference, :amount_paid, :payment_date, :settlement_date, 
             :branch_name, :bank_name, :receipt_no, :payment_currency
  belongs_to :student
end