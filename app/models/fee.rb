class Fee < ApplicationRecord
  belongs_to :student
  validates_presence_of :item_code, :amount_due
  validates_presence_of :channel_name, :payment_method, :amount_paid, :payment_date, :settlement_date,
                        :receipt_no, on: :update
  validates :bill_no, presence: true, uniqueness: true
  validates :payment_log_id, presence: true, uniqueness: true, on: :update
  validates :payment_reference, presence: true, uniqueness: true, on: :update

  def self.all_fees
    self.all.order(:payment_date)
  end

  def self.paid_fees
    self.where('payment_date IS NOT NULL').order(:payment_date)
  end

  def self.unpaid_fees
    self.where('payment_date IS NULL').order(:id)
  end

  def current_amount_due
    self.amount_due - self.amount_paid
  end
end