class Student < ApplicationRecord
  has_one :fee
  has_many :other_payments
  validates_presence_of :last_name, :first_name, :class_name
  validates :admission_no, presence: true, uniqueness: true

  validates :class_name, inclusion: { in: ApplicationHelper::CLASS_NAMES_TO_LUNCH_FEES.keys,
    message: "%{value} is not a valid class name" }

  def self.student_exists?(admission_no)
  	self.where(admission_no: admission_no).exists?
  end
  
  def can_be_deleted
    not self.fee and self.other_payments.size == 0
  end

  def payment_exist? item_code
    self.other_payments.where(item_code: item_code).any?
  end

  def fee_amount_due
    amount_due = self.fee ? self.fee.current_amount_due : 0
  end

  def other_amount_due(item_code)
    amount_paid = self.other_payments.where(item_code: item_code).select('amount_paid').take.to_i
    ApplicationHelper::CLASS_NAMES_TO_LUNCH_FEES['PHD1'] - amount_paid
  end

end
