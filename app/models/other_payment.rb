class OtherPayment < ApplicationRecord
  belongs_to :student, counter_cache: true
  validates_presence_of :item_code, :channel_name, :payment_method, :amount_paid,
                        :payment_date, :settlement_date, :receipt_no
  validates :payment_log_id, presence: true, uniqueness: true
  validates :payment_reference, presence: true, uniqueness: true

  def self.all_payments
    self.all.order(:payment_date)
  end
end
