xml.instruct!
xml.CustomerInformationResponse do 
  xml.MerchantReference(MERCHANT_REFERENCE)
    xml.Customers do
      xml.Customer do
        xml.Status(@validation_data.customer_status)
        xml.CustReference(@student.admission_no)
        xml.CustomerReferenceAlternate
        xml.FirstName(@student.first_name)
        xml.LastName(@student.last_name)
        xml.Email(@student.email)
        xml.Phone(@student.phone)
        xml.ThirdPartyCode
        xml.Amount(@validation_data.amount_due)
        xml.PaymentItems do
          xml.Item do
            xml.ProductName(@validation_data.product_name)
            xml.ProductCode(@validation_data.item_code)
            xml.Quantity(1)
            xml.Price(@validation_data.amount_due)
            xml.Subtotal(@validation_data.amount_due)
            xml.Tax(0)
            xml.Total(@validation_data.amount_due)
          end
        end
      end
    end
  end
end