class FeesController < ApplicationController
  before_action :set_fees, only: [:create]

  def index
    json_response(Fee.all_fees)
  end

  def paid_fees
    json_response(Fee.paid_fees)
  end

  def unpaid_fees
    json_response(Fee.unpaid_fees)
  end

  def create
  	@fees.each do |fee|
  	  set_fee_by_bill_no(fee) 
      set_fee_student_by_admission_no(fee)
      complete_fee_creation(fee)
    end
    render json: { count: @fees.count, status: :created }.to_json
  end

  private

  def set_fees
    @fees = params["fees"]
  end

  def set_fee_by_bill_no(fee)
    @fee = Fee.find_by_bill_no(fee["bill_no"])
  end

  def set_fee_student_by_admission_no(fee)
  	@student = Student.find_by_admission_no(fee["admission_no"])
  end

  def complete_fee_creation(fee)
    raise FeeWithoutStudentError if @student.nil?
    if @fee.nil?
      create_new_fee(fee)       
    else
      @fee.update!(fee_params(fee))
    end
  end

  def create_new_fee(fee)
    @fee = Fee.new(fee_params(fee))
    @fee.amount_paid = 0
    @fee.student = @student
    @fee.save!
  end

  def fee_params(multi_params)
    multi_params.permit :student_id, :item_code, :bill_no, :payment_log_id, :channel_name,
                        :payment_method, :payment_reference, :amount_due, :amount_paid, :payment_date,
                        :settlement_date, :branch_name, :bank_name, :receipt_no, :payment_currency
  end
end
