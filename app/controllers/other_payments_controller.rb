class OtherPaymentsController < ApplicationController
  def index
    json_response(OtherPayment.all_payments)
  end
end
