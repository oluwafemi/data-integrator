module ExceptionHandler
  # provides the more graceful `included` method
  extend ActiveSupport::Concern
  include CustomExceptions

  #included do
  #  rescue_from ActiveRecord::RecordNotFound do |e|
  #    json_response({ message: e.message }, :not_found)
  #  end

  #  rescue_from ActiveRecord::RecordInvalid do |e|
  #    json_response({ message: e.message }, :unprocessable_entity)
  #  end
  #end

  included do
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response
    rescue_from UndeletableStudentError, with: :render_error_response
    rescue_from CustomAttributeError, with: :render_error_response
    rescue_from UnknownRequestError, with: :render_error_response
  end

  def render_unprocessable_entity_response(exception)
    render json: {
      message: "Validation Failed",
      errors: ValidationErrorsSerializer.new(exception.record).serialize
    }, status: :unprocessable_entity
  end

  def render_not_found_response
    render json: { message: "Not found", code: "not_found" }, status: :not_found
  end
  
  def render_error_response(exception)
    render json: { message: exception.message, code: exception.code }, status: exception.http_status
  end
end