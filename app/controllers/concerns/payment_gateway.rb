class PaymentGateway
  attr_accessor :student, :admission_no, :merchant_reference, :status, :item_code, :product_name,
                :amount_due, :payment_log_id, :is_reversal

  def xmlfied_notication_response
    notication_response = { Payments: {
                              Payment: {
                                PaymentLogId: self.payment_log_id,
                                Status: self.status
                              }
                            }
                          }
    notication_response.to_xml(root: "PaymentNotificationResponse")
  end

  def xmlfied_validation_response
  	validation_response = { MerchantReference: self.merchant_reference, 
                            Customers: { 
                              Customer: {
                                Status: self.status, 
                                CustReference: self.student.admission_no,
                                CustomerReferenceAlternate: nil, 
                                FirstName: self.student.first_name,
                                LastName: self.student.last_name, 
                                Email: self.student.email,
                                Phone: self.student.phone,
                                ThirdPartyCode: nil,
                                Amount: self.amount_due,
                                PaymentItems: {
                                  Item: {
                                    ProductName: self.product_name,
                                    ProductCode: self.item_code,
                                    Quantity: 1,
                                    Price: self.amount_due,
                                    Subtotal: self.amount_due,
                                    Tax: 0,
                                    Total: self.amount_due
                                  }
                                }
                              }
                            }
                          }
    validation_response.to_xml(root: "CustomerInformationResponse")
  end
end