module CustomExceptions

  class UnknownRequestError < StandardError

    def http_status
      400
    end

    def code
      'unknown_request'
    end

    def message
      "Your request cannot be handled by the service"
    end

    def to_hash
      {
        message: message,
        code: code
      }
    end
  end

  class UndeletableStudentError < StandardError

    def http_status
      422
    end

    def code
      'undeletable_student'
    end

    def message
      "Students with fees or payments information cannot be deleted"
    end

    def to_hash
      {
        message: message,
        code: code
      }
    end
  end

  class FeeWithoutStudentError < StandardError

    def http_status
      422
    end

    def code
      'fee_without_student'
    end

    def message
      "All fees must have an existing student record"
    end

    def to_hash
      {
        message: message,
        code: code
      }
    end
  end

  class CustomAttributeError < StandardError

    def initialize(msg="One or more attributes is missing from your request")
      super
    end

    def http_status
      400
    end

    def code
      'attribute_missing'
    end

    def to_hash
      {
        message: message,
        code: code
      }
    end
  end

end