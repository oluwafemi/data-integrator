require "rexml/document"
include REXML 

class StudentsController < ApplicationController
  before_action :set_students, only: [:create]
  before_action :set_payment_gateway, only: [:validation_notification]

  def show
    render json: @student, include: ['fees', 'other_payments']
  end

  def create
  	@students.each do |student|
      set_student_by_admission_no(student)
      complete_student_creation(student)
    end
    render json: { count: @students.count, status: :created }.to_json
  end

  def update
    set_student_by_admission_no(params)
    @student.update!(student_params(params))
    head :no_content
  end

  def destroy
    raise UndeletableStudentError unless @student.can_be_deleted
    @student.destroy
    head :no_content
  end

  def validation_notification
    document = Document.new(request.body)
    request_name = XPath.first(document, "/CustomerInformationRequest").to_s
    if request_name.empty?
      request_name = XPath.first(document, "/PaymentNotificationRequest").to_s
      raise UnknownRequestError if request_name.empty?
      process_payment_notification(document)
    else
      process_data_validation(document)
    end
  end

  private

  def set_student
    @student = Student.find(params[:id])
  end

  def set_students
    @students = params["students"]
  end

  def set_payment_gateway
    @payment_gateway = PaymentGateway.new
  end

  def set_student_by_admission_no(student)
  	@student = Student.find_by_admission_no(student["admission_no"])
  end

  def complete_student_creation(student)
    if @student.nil?
      Student.create!(student_params(student))
    else
      @student.update!(student_params(student))
    end
  end

  # DATA-VALIDATION HANDLERS

  def process_data_validation(document)
    fill_validation_test_attributes(document)
    if @payment_gateway.merchant_reference == MERCHANT_REFERENCE and ITEM_CODES.include?(@payment_gateway.item_code)
      @student = Student.find_by_admission_no(@payment_gateway.admission_no)
      if @student.nil?
        build_invalid_validation_request
      else
        build_valid_validation_request
      end
    else
      build_invalid_validation_request
    end
    render xml: @payment_gateway.xmlfied_validation_response   
  end

  def fill_validation_test_attributes(document)
    @payment_gateway.merchant_reference = XPath.first(document, "/CustomerInformationRequest/MerchantReference").get_text.to_s
    @payment_gateway.admission_no = XPath.first(document, "/CustomerInformationRequest/CustReference").get_text.to_s
    @payment_gateway.item_code = XPath.first(document, "/CustomerInformationRequest/PaymentItemCode").get_text.to_s
  end

  def build_valid_validation_request
    @payment_gateway.student = @student
    @payment_gateway.status = VALID_CUSTOMER_STATUS
    @payment_gateway.product_name = ITEM_CODES_TO_PRODUCT_NAMES[@payment_gateway.item_code]
    @payment_gateway.amount_due = (FEES_ITEM_CODE == @payment_gateway.item_code) ?
                                    @student.fee_amount_due :
                                    @student.other_amount_due(@payment_gateway.item_code)
  end

  def build_invalid_validation_request
    @payment_gateway.status = INVALID_CUSTOMER_STATUS
    @payment_gateway.student = Student.new
  end

  # PAYMENT NOTIFICATION HANDLERS

  def process_payment_notification(document)
    fill_notification_test_attributes(document)
    if @payment_gateway.is_reversal == 'false' and ITEM_CODES.include?(@payment_gateway.item_code)
      @student = Student.find_by_admission_no(@payment_gateway.admission_no)
      if @student.nil?
        build_rejected_payment_response
      else
        complete_payment_notification(document)
      end
    else 
      build_rejected_payment_response
    end
    render xml: @payment_gateway.xmlfied_notication_response
  end

  def fill_notification_test_attributes(document)
    @payment_gateway.admission_no = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/CustReference").get_text.to_s
    @payment_gateway.item_code = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/PaymentItems/PaymentItem/ItemCode").get_text.to_s
    @payment_gateway.is_reversal = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/IsReversal").get_text.to_s.downcase!
  end

  def complete_payment_notification(document)
    if @payment_gateway.item_code == FEES_ITEM_CODE
      handle_fee_notification(document)
    else
      handle_other_payments_notification(document)
    end
  end

  def handle_fee_notification(document)
    fee = @student.fee
    if fee.nil?
      build_rejected_payment_response
    else
      fee.amount_paid = extract_amount_paid(document)
      if fee.amount_paid == fee.amount_due
        fee = fill_payment_attributes(fee, document)
        fee.save
      else
        build_rejected_payment_response
      end
    end
  end

  def handle_other_payments_notification(document)
    if @student.payment_exist?(@payment_gateway.item_code)
      build_rejected_payment_response
    else
      other_payment = OtherPayment.new
      other_payment.student_id = @student.id
      other_payment.item_code = @payment_gateway.item_code
      other_payment.amount_paid = extract_amount_paid(document)
      if other_payment.amount_paid == CLASS_NAMES_TO_LUNCH_FEES[@student.class_name]
        other_payment = fill_payment_attributes(other_payment, document)
        other_payment.save
      else
        build_rejected_payment_response
      end
    end
  end

  def extract_amount_paid(document)
    XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/Amount").get_text.to_s.remove!('.').to_i
  end

  def fill_payment_attributes(payment, document)
    payment.payment_log_id = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/PaymentLogId").get_text.to_s.to_i
    payment.channel_name = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/ChannelName").get_text.to_s
    payment.payment_method = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/PaymentMethod").get_text.to_s
    payment.payment_reference = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/PaymentReference").get_text.to_s
    
    payment_date = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/PaymentDate").get_text.to_s
    payment.payment_date = DateTime.strptime(payment_date, "%m/%d/%Y %H:%M:%S")
    
    settlement_date = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/SettlementDate").get_text.to_s
    payment.settlement_date = DateTime.strptime(settlement_date, "%m/%d/%Y %H:%M:%S")

    payment.branch_name = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/BranchName").get_text.to_s
    payment.bank_name = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/BankName").get_text.to_s
    payment.receipt_no = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/ReceiptNo").get_text.to_s
    payment.payment_currency = XPath.first(document, "/PaymentNotificationRequest/Payments/Payment/PaymentCurrency").get_text.to_s
    @payment_gateway.status = payment.valid? ? PAYMENT_SUCCESSFULLY_RECEIVED : PAYMENT_REJECTED
    @payment_gateway.payment_log_id = payment.payment_log_id
    payment
  end

  def build_rejected_payment_response
    @payment_gateway.status = PAYMENT_REJECTED
  end

  def student_params(multi_params)
    multi_params.permit :admission_no, :last_name, :first_name, :other_name, 
                        :email, :phone, :class_name
  end

end
