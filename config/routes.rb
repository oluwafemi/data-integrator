Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope '/api' do
    scope '/v1' do

      post 'validation_notification', to: 'students#validation_notification'
  	  
      resources :students, only: [:create, :show, :index, :update, :destroy]
      
      resources :fees, only: [:create, :index] do
        collection do
          get 'paid_fees'
          get 'unpaid_fees'
        end
      end
        
      resources :other_payments, only: [:index]

    end
  end
end
