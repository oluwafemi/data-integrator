class CreateOtherPayments < ActiveRecord::Migration[5.1]
  def change
    create_table :other_payments do |t|
      t.references :student, foreign_key: true
      t.string :item_code, null: false
      t.string :channel_name, null: false
      t.string :payment_method, null: false
      t.string :payment_reference, null: false 
      t.integer :payment_log_id, null: false
      t.bigint :amount_paid, null: false
      t.timestamp :payment_date, null: false
      t.timestamp :settlement_date, null: false
      t.string :branch_name
      t.string :bank_name
      t.string :receipt_no, null: false
      t.string :payment_currency
      t.timestamps
    end
    add_index :other_payments, [:student_id, :item_code], unique: true, name: 'student_item_code_idx'
    add_index :other_payments, :payment_log_id, unique: true, name: 'others_payment_log_id_idx'
    add_index :other_payments, :payment_reference, unique: true, name: 'others_payment_reference_idx'
  end
end
