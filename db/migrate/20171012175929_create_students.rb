class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :admission_no, null: false
      t.string :last_name, null: false
      t.string :first_name, null: false
      t.string :other_name
      t.string :email
      t.string :phone
      t.string :class_name, null: false
      t.integer :other_payments_count
      
      t.timestamps
    end
    add_index :students, :admission_no, unique: true, name: 'admission_no_idx'
    add_index :students, [:last_name, :first_name, :other_name], unique: true, name: 'last_first_other_names_idx'
  end
end
