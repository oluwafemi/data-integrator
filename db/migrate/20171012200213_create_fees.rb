class CreateFees < ActiveRecord::Migration[5.1]
  def change
    create_table :fees do |t|
      t.references :student, foreign_key: true
      t.string :item_code, null: false
      t.string :bill_no, null: false
      t.integer :payment_log_id
      t.string :channel_name
      t.string :payment_method
      t.string :payment_reference
      t.bigint :amount_due, null: false
      t.bigint :amount_paid
      t.timestamp :payment_date
      t.timestamp :settlement_date
      t.string :branch_name
      t.string :bank_name
      t.string :receipt_no
      t.string :payment_currency
      t.timestamps
    end
    add_index :fees, :bill_no, unique: true, name: 'bill_no_idx'
    add_index :fees, :payment_log_id, unique: true, name: 'fees_payment_log_id_idx'
    add_index :fees, :payment_reference, unique: true, name: 'fees_payment_reference_idx'
  end
end
