# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171012200243) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "fees", force: :cascade do |t|
    t.bigint "student_id"
    t.string "item_code", null: false
    t.string "bill_no", null: false
    t.integer "payment_log_id"
    t.string "channel_name"
    t.string "payment_method"
    t.string "payment_reference"
    t.bigint "amount_due", null: false
    t.bigint "amount_paid"
    t.datetime "payment_date"
    t.datetime "settlement_date"
    t.string "branch_name"
    t.string "bank_name"
    t.string "receipt_no"
    t.string "payment_currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bill_no"], name: "bill_no_idx", unique: true
    t.index ["payment_log_id"], name: "fees_payment_log_id_idx", unique: true
    t.index ["payment_reference"], name: "fees_payment_reference_idx", unique: true
    t.index ["student_id"], name: "index_fees_on_student_id"
  end

  create_table "other_payments", force: :cascade do |t|
    t.bigint "student_id"
    t.string "item_code", null: false
    t.string "channel_name", null: false
    t.string "payment_method", null: false
    t.string "payment_reference", null: false
    t.integer "payment_log_id", null: false
    t.bigint "amount_paid", null: false
    t.datetime "payment_date", null: false
    t.datetime "settlement_date", null: false
    t.string "branch_name"
    t.string "bank_name"
    t.string "receipt_no", null: false
    t.string "payment_currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["payment_log_id"], name: "others_payment_log_id_idx", unique: true
    t.index ["payment_reference"], name: "others_payment_reference_idx", unique: true
    t.index ["student_id", "item_code"], name: "student_item_code_idx", unique: true
    t.index ["student_id"], name: "index_other_payments_on_student_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "admission_no", null: false
    t.string "last_name", null: false
    t.string "first_name", null: false
    t.string "other_name"
    t.string "email"
    t.string "phone"
    t.string "class_name", null: false
    t.integer "other_payments_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admission_no"], name: "admission_no_idx", unique: true
    t.index ["last_name", "first_name", "other_name"], name: "last_first_other_names_idx", unique: true
  end

  add_foreign_key "fees", "students"
  add_foreign_key "other_payments", "students"
end
